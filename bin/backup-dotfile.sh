#!/bin/env sh
# Backup dotfiles with rsync

# .gnome/apps contains google-chrome apps
# .var/app contains Flatpak apps

# Some cleanup
rm -rf ~/.ansible
# Created by Jetbrains
rm -rf ~/.android

RSYNC_DST=/run/media/bruno/B1Tera/Bruno/backup
# TEST="--dry-run --verbose"

rsync $TEST -P -r -l -t --exclude-from=-  $HOME/.[^.]* $RSYNC_DST/dot_$(date +"%F")/ << EOI
/.cache/
/.var/
/.share/m2/repository/
/.gnome/
/.mozilla/
/.vscode*/
/.vim/
/.config/google-c*/
/.config/Code*/
/.config/libvirt/qemu/lib/
/.config/libvirt/qemu/save/
/.config/pulse/
/.config/oh-my-zsh/
/.config/vcsh/
/.local/share/
/.local/lib/
/.local/share/flatpak/repo/
/.hydrogen/
/.java/
/.zoom/
EOI


rsync $TEST -P -t -b --suffix="-$(date +%F)" --files-from=-  / $RSYNC_DST/ << EOI
/etc/ansible/ansible.cfg
/etc/security/limits.conf
EOI

#/etc/pipewire/
#/etc/pipewire/media-session.d/
