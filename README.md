# Dotfiles
Tracking some configuration files via Git

Using https://github.com/RichiH/vcsh allows to use git in $HOME directory without having everything in git. You kind of "enter" the git world only when you want. 
(It can also put different files in different repositories. I could have use another repository for the `bin/` folder, one repo for `tmux` ... They use `myrepos` for this).

## Installation 

Copy Paste install
```
sudo dnf install vcsh zsh sqlite tree git curl fzf screen xz htop
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Optional, to avoid error message "fetching gitstatusd ..."
~/.config/oh-my-zsh/custom/themes/powerlevel10k/gitstatus/install
```

NOTE: No need to install `VS Code` in WSL https://code.visualstudio.com/docs/remote/wsl


## Usage

Notes:
- `vcsh write-gitignore <MyRepo>` will ignore all the untracked files (very handy)
- `vcsh <MyRepo>`  will "enter" into git world: you can issue git command directly. (`exit` to go back) 
- `git ls-files` list all tracked files
- To remove a file from the index: `git rm --cached ~/.config/oh-my-zsh/oh-my-zsh.sh`


Ref:
- https://GitLab.com/BrunoVernay/Dotfiles 

